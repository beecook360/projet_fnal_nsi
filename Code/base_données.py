# -*- coding: utf-8 -*-

lieus = {
        '1':'Troie',
        '2':'Cicones',
        '3':'Lotophages',
        '4':'Cyclope',
        '5':'Eole',
        '6':'Lestrygeons',
        '7':'Circé',
        '8':'Sirènes',
        '9':'Charybde et Scylla',
        '10':'Boeufs sacré',
        '11':'Calypso',
        '12':'Phéaciens',
        '13':'Ithaque',
        }

stats = {'day': 0, 'malus': 0, 'benediction' : False, 'richesse' : False, 'bravoure' : False, 'guerrier' : False}

