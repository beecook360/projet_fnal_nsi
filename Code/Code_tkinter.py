# -*- coding: utf-8 -*-

from tkinter import *
import pygame
from PIL import Image, ImageTk

# Change la fenêtre pour avoir la fenetre des consignes
def consignes():
    titre['bg'] = '#139FB0'
    titre['font'] = ('Arial', 20)
    titre['text'] = "Notre jeu est un jeu d'aventure où l'on incarne le personnage d'Ulysse lors de l'Odyssée. \nLe but est de faire le chemin du retour de la guerre de Troie le plus rapidement possible. \nBonne chance et faites les bons choix ou riquez de perdre votre bien aimée ! "
    accueil.config(background='#139FB0')
    
    # Permet de jouer de la musique en fond
    pygame.mixer.init()
    pygame.mixer.music.load("Sources/Musique.mp3")
    pygame.mixer.music.play(loops=0)
    
    bouton['text'] = 'Cliquez pour jouer'
    bouton['command'] = game
    
# Change la fenetre pour avoir la fenetre de jeu
def game():
    
    # Destruction du bouton et du texte
    bouton.destroy()
    titre.destroy()
    
    # Arrete la musique
    pygame.mixer.music.stop()
    
    boite = Frame(accueil, bg='#139FB0')
    score = Label(boite, text='Les scores       ', font=('Arial', 15), bg='#139FB0', fg='black')
    score.pack(side=LEFT)
    avancement = Label(boite, text="L'avancement", font=('Arial', 15), bg='#139FB0', fg='black')
    avancement.pack(side=RIGHT)
    boite.place(x=5, y=0)
    
    carte = Image.open('Sources/Carte.png')
    carte_resized = carte.resize((1550, 800))
    carte_bg = ImageTk.PhotoImage(carte_resized)
    
    canvas = Canvas(accueil, width=1555, height=800)
    canvas.create_image(0, 0, anchor=NW, image=carte_bg)
    canvas.place(x=0, y=0)
    
    
accueil = Tk()
accueil.title("L'odyssée d'Ulysse")
accueil.geometry("1080x720")


ulysse = Image.open('Sources/Ulysse.png')
ulysse_resized = ulysse.resize((1550, 800))
ulysse_bg = ImageTk.PhotoImage(ulysse_resized)
    
canvas = Canvas(accueil, width=1555, height=800)
canvas.create_image(0, 0, anchor=NW, image=ulysse_bg)
canvas.place(x=0, y=0)

titre = Label(accueil, text='Bienvenue sur notre jeu ! ', font=('Arial', 40))
titre.pack(expand=TRUE)


bouton = Button(accueil, text='Cliquez pour commencer', font=('Arial', 20),
                fg='black', bg='yellow', command=consignes)
bouton.pack(expand=TRUE)

accueil.mainloop()